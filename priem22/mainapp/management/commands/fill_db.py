from django.core.management.base import BaseCommand
from mainapp.models import Abiturient, Specialization
from django.contrib.auth.models import User

import json, os

JSON_PATH = 'mainapp/jsons'


def load_from_json(file_name):
    with open(os.path.join(JSON_PATH, file_name + '.json'), 'r') as infile:
        return json.load(infile)


class Command(BaseCommand):
    def handle(self, *args, **options):
        specializations = load_from_json('specialization')
        Specialization.objects.all().delete()
        for item in specializations:
            new_specialization = Specialization(**item)
            new_specialization.save()

        abiturients = load_from_json('abiturient')
        Abiturient.objects.all().delete()
        for item in abiturients:
            new_abiturient = Abiturient(**item)
            new_abiturient.save()

        super_user = User.objects.create_superuser('admin', 'agk-it@yandex.ru', 'E5H2xL334')
