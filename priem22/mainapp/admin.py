from django.contrib import admin
from .models import Specialization, Abiturient

admin.site.register(Abiturient)
admin.site.register(Specialization)

# Register your models here.
