from django.db import models


class Specialization(models.Model):
    name = models.CharField(
        verbose_name='Название специальности',
        max_length=120,
        unique=True
    )
    description = models.TextField(
        verbose_name='Описание специальности',
        blank=True,
    )
    limit_bg = models.IntegerField(
        verbose_name='Размер набора на бюджет',
        default=None
    )
    limit_pay = models.IntegerField(
        verbose_name='Размер набора на внебюджет',
        default=None
    )
    is_active = models.BooleanField(
        verbose_name='активна',
        default=True
    )

    def __str__(self):
        return self.name


class Abiturient(models.Model):
    lastname = models.CharField(
        verbose_name='Фамилия',
        max_length=32,
        unique=False
    )
    firstname = models.CharField(
        verbose_name='Имя',
        max_length=32,
        unique=False
    )
    midlename = models.CharField(
        verbose_name='Отчество',
        max_length=32,
        unique=False,
        blank=True
    )
    birthday = models.DateField(
        verbose_name='Дата рождения',
        unique=False
    )
    avg_ball = models.FloatField(
        verbose_name='Средний бал',
        default=None
    )
    special = models.ForeignKey(
        Specialization,
        on_delete=models.CASCADE
    )

    created = models.DateTimeField(
        auto_now=True
    )

    updated = models.DateTimeField(
        auto_now=True
    )

    def __str__(self):
        return f'{self.lastname} {self.firstname} {self.midlename}'
