# Generated by Django 4.0.5 on 2022-06-20 13:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='abiturient',
            name='avg_ball',
            field=models.FloatField(default=None, verbose_name='Средний бал'),
        ),
        migrations.AddField(
            model_name='specialization',
            name='limit_bg',
            field=models.IntegerField(default=None, verbose_name='Размер набора на бюджет'),
        ),
        migrations.AddField(
            model_name='specialization',
            name='limit_pay',
            field=models.IntegerField(default=None, verbose_name='Размер набора на внебюджет'),
        ),
    ]
