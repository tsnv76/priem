from django.shortcuts import render
from datetime import date

from mainapp.models import Abiturient


def main(request):
    title = 'Главная'
    abit = Abiturient.objects.all()
    len_items = len(abit)
    context = {
        'title': title,
    }
    return render(request, 'mainapp/index.html', context)


def abit(request):
    title = 'Абитуриент'

    abit = Abiturient.objects.all()
    len_items = len(abit)

    today_item = 0

    for item in abit:
        now = item.created.date()
        # print(now.day, '  ', date.today().day)
        if now.day == date.today().day:
            today_item += 1

    # print(len_items)
    context = {
        'title': title,
        'today_item': today_item,
        'items_list': abit,
        'len_items': len_items,
    }
    return render(request, 'mainapp/abit.html', context)


def otchets(request):
    title = 'Отчеты'
    context = {
        'title': title,
        'links_menu': '',
        'category': '',
        'products': '',
    }
    return render(request, 'mainapp/otchets.html', context)


def add_abit(request):
    title = 'Добавление нового абитуриента'
    context = {
        'title': title,
        'links_menu': '',
        'category': '',
        'products': '',
    }
    return render(request, 'mainapp/add_abit.html', context)


def edit_abit(request):
    title = 'Редактирование информации об абитуриенте'
    context = {
        'title': title,
        'links_menu': '',
        'category': '',
        'products': '',
    }
    return render(request, 'mainapp/edit_abit.html', context)


def delete_abit(request):
    title = 'Удаление абитуриента'
    # context = {
    #     'title': title,
    #     'links_menu': '',
    #     'category': '',
    #     'products': '',
    # }
    # return render(request, 'mainapp/edit_abit.html', context)
